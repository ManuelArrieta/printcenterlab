﻿using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureHostConfiguration(configHost =>
    {
        configHost.SetBasePath(Directory.GetCurrentDirectory());
        configHost.AddJsonFile($"appsettings.json", optional: false);
        configHost.AddEnvironmentVariables();
    })
    .ConfigureServices((host, services) =>
    {
        services.AddHangfire(config =>
            config.SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
                .UseColouredConsoleLogProvider()
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings());
    })
    .Build();

IConfiguration configuration = host.Services.GetRequiredService<IConfiguration>();
GlobalConfiguration.Configuration
    .UseRedisStorage(
        configuration["ConnectionStrings:RedisConnectionString"],
        new RedisStorageOptions { Prefix = "hangfire:printcenter:jobs:" });
