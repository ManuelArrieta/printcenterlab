﻿await Host.CreateDefaultBuilder(args)
    .ConfigureHostConfiguration(
        configHost =>
        {
            configHost.SetBasePath(Directory.GetCurrentDirectory());
            Console.WriteLine(Directory.GetCurrentDirectory());
            configHost.AddJsonFile(
                $"appsettings.json", optional: false
            );
            configHost.AddEnvironmentVariables();
        })
    .ConfigureServices(
        (host, services) =>
        {
            services.AddHangfire(
                config =>
                    config.SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
                        .UseColouredConsoleLogProvider()
                        .UseSimpleAssemblyNameTypeSerializer()
                        .UseRecommendedSerializerSettings()
                        .UseRedisStorage(
                            host.Configuration["ConnectionStrings:RedisConnectionString"],
                            new RedisStorageOptions { Prefix = "hangfire:printcenter:jobs:" }
                        )
            );
            services.AddHangfireServer();
        })
    .RunConsoleAsync();