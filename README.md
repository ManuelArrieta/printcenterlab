# AEAT - CENTRO DE IMPRESIÓN

### Consideraciones Previas
Para este laboratorio tendremos en cuenta los siguientes conceptos
- Kafka:
  - Implementacion de productor y consumidor
- HangFire:
  - Implementacion de 'Jobs' de segundo plano para la gestion de tareas.

Actualmente el laboratorio cuenta con un archivo docker-compose que levanta 3 servicios:
- Servicio de zookeeper
- Servicio de kafka [Puertos:9092,9093]
- Servicio de redis [Puerto:6379]

### Contextualizacion

La **Agencia Estatal de Administración Tributaria** (AEAT) ha contratado a SmartwayStudios
con el fin de optimizar el proceso de impresiones para el recaudo
de impuestos nacionales. Actualmente su sistema de facturación va lento y 
requiere que este pueda renderizar la información proporcionada por hacienda y 
adicional pueda enviar notificaciones a los destinatarios correspondientes una 
vez la factura este creada. Por lo anterior SmarwayStudios ha planteado la 
siguiente solución que usted deberá implementar.

<img height="800" src="https://lh3.google.com/u/1/d/1gqvMWZvGkWD9quNYjt8KdRTvxe80pA1B=w1920-h944-iv1" width="1200"/>
Recuerde que el sistema externo del AEAT no debera de implementarse.
Si no visualiza la imagen puede dirigirse al siguiente link:
[Imagen](https://lh3.google.com/u/1/d/1gqvMWZvGkWD9quNYjt8KdRTvxe80pA1B=w1920-h944-iv1)


### Detalles de la solucion

### Kafka

El **topic** que contendra los mensajes enviados sera el siguiente:
* `h-fact-v01`

La estructura del mensaje publicado sobre el bus de eventos tendra la siguiente estructura:
```Json
{
  "Jname":"json-response",
  "Jversion":"0.0.1",
  "billingProcessID":"11457",
  "request":"{ \"Jname\" : \"json-cycle\", \"Jversion\" : \"0.0.1\", \"processID\" : \"\", \"typeID\" : \"5\", \"accountID\": \"F9850FFB-C471-01CF-E053-030011ACAADF\", \"periodID\" : \"10523\" , \"flagSimulation\" : \"0\"",
  "executionDate":"2023-07-14T10:38:53",
  "client": "",
  "error":"0",
  "detail":"",
  "customerName": "Artilleria estatal de murcia",
  "customerEmail": "aedm@gov.es",
  "invoiceID":"1",
  "URL":"/localSimulationPath/customerName/file.json"
}
```
En la estructura del mensaje nos enfocaremos especificamente en 5 campos:
* `invoiceID` : Los archivos generados por el servicio debera de tener por nombre
el numero de la factura.
* `customerName`:El nombre del cliente se utilizara para la creacion de la carpeta que contendra
todas las facturas del cliente.
* `customerEmail`:El email del cliente lo utilizaremos para la simulacion de notificacion por correo.
* `URL`: La url no es mas que un path local (donde usted desee) donde estara el archivo que contendra la
.json que contendra todos los datos nesesarios para crear el archivo .pdf. (mas adelante entraremos en los detalles de la estructura de este archivo.)
* `error`:Este campo si es diferente de '0'(cero) no se habra generado ningun tipo de archivo .json y debera de manejarlo con una excepcion(nueva) creada por usted.debera adicional
visualizarlo con 'Logger' que viene implementado e inyectado por defecto en .NET y cuya interfaz es `ILOGGER.

  
Recuerde que el path local podra estar en cualquier parte de su ordenador. Adicional dentro
de este path ya deberan existir previamente las carpetas con los archivos .json que usted debera
de leer ejemplo:
* `/usr/pepito/Desktop/Smartway/PrintCenterLab/tmp/(customerNameA)/(invoiceID1).json`
* `/usr/pepito/Desktop/Smartway/PrintCenterLab/tmp/(customerNameB)/(invoiceID2).json`
* `/usr/pepito/Desktop/Smartway/PrintCenterLab/tmp/(customerNameC)/(invoiceID3).json`

Recuerde que la instancia de `Queue` estara siempre escuchando el bus de eventos de kafka esperando
el mensaje que que le suministrara la informacion nesesaria para llegar a estos archivos. Ademas recuerde que
sobre este mismo `path` se generaran los pdf. A continuacion un ejemplo de la estructura de los archivos .json que
estaran ubicados en tales rutas.

```Json
{
  "data":"(pdf:base64)"
}
```

El archivo para todos sera el mismo, durante las pruebas cuando se emita o produzca un evento en el bus de eventos
lo unico que cambiara sera el `invoiceID` y si se estan probando diferentes clientes entonces cambiara el `customerName`.

Dentro del proyecto habra un archivo .json de ejemplo que es el que se utilizara para la simulacion. El nombre
del archivo es el siguiente ``pdfData.json``

###Nota:
Recuerde que la capa de ``Queue`` solo debera de encolar el trab. Esta instancia no es la encargada de la renderizacion.

### HangFire

Para la capa de ``Jobs`` recuerde que esta instancia es donde estara corriendo el servidor de HangFire y sera el encargado de la
renderizacionde la informacion leida en la ruta del mensaje y que posteriormente sera la encarga de encolar un segundo trabajo de notificacion.

### Controllers

Para la capa de ``Host`` implementaremos un metodo asincrono llamado SendMessage cuyo parametro sera un DTO con la
estructura del mensaje anteriormente propuesto. Este metodo sera el encargado de simular el envio al bus de eventos el mensaje
anteriormente propuesto y este tendra que devolver IActionResult una vez el mensaje sea enviado; Ok si salio bien de lo
contrario un NotFound.

### Otras consideraciones

Durante su implementacion, recuerde que es libre de organizar su desarrollo
deacuerdo a las capas que actualmente cuenta la solucion teniendo en cuenta su propia interpretacion. Ademas 
No sera nesesario el uso de todas estas a menos que usted lo considere nesesario. Recuerde que el desarrollo debera de tener una coherencia entre la logica implementada
y su hubicacion dentro de la solucion. Adicional las capas de ``Host`` , ``Jobs``y ``Queue`` 
ya se encuentran minimamente configuradas para su uso.