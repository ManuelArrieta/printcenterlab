global using Microsoft.Extensions.Configuration;
global using Microsoft.Extensions.DependencyInjection;
global using Confluent.Kafka;
global using Microsoft.Extensions.Hosting;
global using Hangfire;
global using Hangfire.Pro.Redis;